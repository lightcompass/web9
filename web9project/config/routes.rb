Rails.application.routes.draw do
  resources :comments
  get 'admin/index'
  resources :posts
  devise_for :users
  root 'posts#index'
  get 'admin/ban'
  get 'admin/approve'
  get 'admin/unban'
  get 'admin/deletepost'
  resources :posts do
    resources :comments
  end

  mount SimpleDiscussion::Engine => "/forum"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
