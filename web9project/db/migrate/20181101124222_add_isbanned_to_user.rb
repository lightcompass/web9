class AddIsbannedToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :isbanned, :boolean, default: false
  end
end
