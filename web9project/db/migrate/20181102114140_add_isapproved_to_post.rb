class AddIsapprovedToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :isapproved, :boolean, :default => false
  end
end