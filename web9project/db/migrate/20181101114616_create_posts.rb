class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :description
      t.string :address
      t.string :cost
      t.string :email
      t.float :latitude
      t.float :longitude
      t.string :contactname
      t.string :apartmentorroom
      t.integer :numberofroom
      t.string :phone

      t.timestamps
    end
  end
end
