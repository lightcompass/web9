# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: "part@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            name: "Part Jaithong",
            address: "Bangkok, Thailand",
            phone: "0622998332",
            sex: "Male",
            occupation: "Rails Developer",
            annualincome: "$120000",
            dob: "12/08/1991",
            maritalstatus: "Single",
            username: "part123"
)
User.create(email: "joe@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            name: "Joe Simmons",
            address: "Bangkok, Thailand",
            phone: "0622998332",
            sex: "Male",
            occupation: "Ruby Developer",
            annualincome: "$150000",
            dob: "17/05/1994",
            maritalstatus: "Single",
            username: "joe123"
)

User.create(email: "admin@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            name: "Abhishek Koirala",
            address: "AIT, Pathum Thani, Thailand",
            phone: "0622565656",
            sex: "Male",
            occupation: "System Developer",
            annualincome: "$300000",
            dob: "21/09/1992",
            maritalstatus: "Single",
            username: "admin123",
            isadmin: true
)

Post.create(title: "Apartment at PathumThani",
            description: "A very family oriented apartment is available near AIT (Well Furnished)",
            address: "Khlong Luang, Pathum Thani, AIT",
            cost: "12000 Bhat/month",
            email: "part@gmail.com",
            latitude: 14.0785,
            longitude: 100.6140,
            contactname: "Part Jaithong",
            apartmentorroom: "Apartment",
            numberofroom: 4,
            phone: "0622998332",
            user_id: 1,
            isapproved: false
)