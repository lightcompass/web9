class Post < ApplicationRecord
  has_many :comments, dependent: :destroy

  has_attached_file :image, styles: { large: "600X600>", medium: "300X300>", thumb:"64X64#"}
  validates_attachment_content_type :image, content_type: /\image\/.*\Z/
end
