class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_attached_file :image, styles: { large: "600X600>", medium: "300X300>", thumb:"64X64#"}
  validates_attachment_content_type :image, content_type: /\image\/.*\Z/


  include SimpleDiscussion::ForumUser

  # def name
  #   "#{first_name} #{last_name}"
  # end

end
