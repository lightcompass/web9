json.extract! post, :id, :title, :description, :address, :cost, :email, :latitude, :longitude, :contactname, :apartmentorroom, :numberofroom, :phone, :created_at, :updated_at
json.url post_url(post, format: :json)
