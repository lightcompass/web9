class AdminController < ApplicationController
  def index
    # getting all user list
    @users = User.all
    @count = @users.where('isbanned = :yes',
                          :yes => true,
    ).count
    # getting number of user registered last month
    @lastthirtydays = User.where(
        'created_at >= :thirty_days_ago',
        :thirty_days_ago => 30.days.ago,
    ).count
    @post = Post.all

  end

  def ban
    # MyModel.where(id: 1).update_all(banned: true)
    @id = params[:userid]
    User.where(:id => @id).update_all(:isbanned => true)
    respond_to do |format|
      format.html {redirect_to admin_index_path}
    end
  end

  def unban
    # MyModel.where(id: 1).update_all(banned: true)
    @id = params[:userid]
    User.where(:id => @id).update_all(:isbanned => false)
    respond_to do |format|
      format.html {redirect_to admin_index_path}
    end
  end

  def approve
    @postid = params[:postid]
    Post.where(:id => @postid).update_all(:isapproved => true)
    respond_to do |format|
      format.html {redirect_to admin_index_path}
    end
  end

  def deletepost
    @postid = params[:postid]
    Post.where(:id => @postid).delete_all
    respond_to do |format|
      format.html {redirect_to admin_index_path}
    end
  end

end
