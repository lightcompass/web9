class ApplicationController < ActionController::Base
  before_action :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :address, :phone, :sex, :occupation, :dob, :maritalstatus, :email, :password, :annualincome, :username, :image])
  end
end
