module ApplicationHelper
  def gender_lists
    I18n.t(:gender_lists).map { |key, value| [ value, key ] }
  end
  def marital_status
    I18n.t(:marital_status).map { |key, value| [ value, key ] }
  end
  def categories
    I18n.t(:categories).map { |key, value| [ value, key ] }
  end

end
