require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    sign_in users(:one)
    get posts_url
    assert_response :success
  end

  test "should get new" do
    sign_in users(:one)
    get new_post_url
    assert_response :success
  end

  test "should create post" do
    sign_in users(:one)
    assert_difference('Post.count') do
      post posts_url, params: {post: {address: @post.address, apartmentorroom: @post.apartmentorroom, contactname: @post.contactname, cost: @post.cost, description: @post.description, email: @post.email, latitude: @post.latitude, longitude: @post.longitude, numberofroom: @post.numberofroom, phone: @post.phone, title: @post.title, user_id: users(:one).id}}
    end

    assert_redirected_to post_url(Post.last)
  end

  test "should show post" do
    sign_in users(:one)
    get post_url(@post)
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:one)
    get edit_post_url(@post)
    assert_response :success
  end

  test "should update post" do
    sign_in users(:one)
    patch post_url(@post), params: {post: {address: @post.address, apartmentorroom: @post.apartmentorroom, contactname: @post.contactname, cost: @post.cost, description: @post.description, email: @post.email, latitude: @post.latitude, longitude: @post.longitude, numberofroom: @post.numberofroom, phone: @post.phone, title: @post.title, user_id: users(:one)}}
    assert_redirected_to post_url(@post)
  end

  test "should destroy post" do

    assert_difference('Post.count', -1) do
      delete post_url(@post)
    end


    assert_redirected_to posts_url
  end


end
