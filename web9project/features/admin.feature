Feature: Student
  The admin login should redirect to admin dashboard where admin can perform following operations

  Scenario: Admin sees recently registered users
  Admin should be able to see recently registered users once he/she logs in
    Given I am an Admin
    And I am signed in
    When I visit the dasboard page
    Then I should see a list of recently registered users

  Scenario: Admin can ban users
  Admin should be able to ban user from user listing
    Given I am an Admin
    And I am signed in
    When I visit the dasboard page
    Then I should see a Ban link on user list
    When I click the ban button
    Then The user list should see isbannedtrue