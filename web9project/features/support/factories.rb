FactoryBot.define do
  factory :admin, class: User do
    email "admin123@gmail.com"
    password "123456"
    password_confirmation "123456"
    name "Abhishek Koirala"
    address "AIT, Pathum Thani, Thailand"
    phone "0622565656"
    sex "Male"
    occupation "System Developer"
    annualincome "$300000"
    dob "21/09/1992"
    maritalstatus "Single"
    username "admin123"
    isadmin true
    isbanned false
  end
  factory :user, class: User do
    email "part@gmail.com"
    password "123456"
    password_confirmation "123456"
    name "Part Jaithong"
    address "AIT, Pathum Thani, Thailand"
    phone "0622565656"
    sex "Male"
    occupation "System Developer"
    annualincome "$300000"
    dob "21/09/1992"
    maritalstatus "Single"
    username "part123"
    isadmin false
    isbanned false
  end
  factory :post, class: Post do
    title "Apartment at PathumThani"
    description "A very family oriented apartment is available near AIT (Well Furnished)"
    address "Khlong Luang, Pathum Thani, AIT"
    cost "12000 Bhat/month"
    email "part@gmail.com"
    latitude 14.0785
    longitude 100.6140
    contactname "Part Jaithong"
    apartmentorroom "Apartment"
    numberofroom 4
    phone "0622998332"
    user_id 1
    isapproved true
  end
end