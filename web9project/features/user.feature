Feature: User
  The User login should redirect to post index where user can perform following operations
  Scenario: User should be able to add a new post
    Given I am a user
    And I am signed in as a user
    And I want to add a post
    When I visit the post page
    Then I should see a link to add post
    When I click the link for the new post
    Then I should see a form to add a post
    When I submit the form
    Then I should see the details of my post
#  Scenario: User can edit their own post
#    Then I should see a link to edit post
#    When I click the link for to edit post
#    Then I should see a form to edit my post
#    When I submit the form
#    Then I should see the details of my post