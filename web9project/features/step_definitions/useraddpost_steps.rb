Given("I am a user") do
  @user = FactoryBot.create :user
end

Given("I am signed in") do
  visit '/'
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
end
Given("I want to add a post") do
  @post=FactoryBot.build :post
end
When("I visit the post page") do
  visit posts_path
end

Then("I should see a link to add post") do
  expect(page).to have_link('New Post', href: new_post_path)
end

When("I click the link for the new post") do
  find_link('New Post', href: new_post_path).click
end

Then("I should see a form to add a post") do
  expect(page).to have_selector('form#new_post')
end

When("I submit the form") do
  # fill_in 'Title', with: @post.title
  # fill_in 'Description', with: @post.description
  # fill_in 'Address', with: @post.address
  # fill_in 'Cost', with: @post.cost
  # fill_in 'Email', with: @post.email
  # fill_in 'Latitude', with: @post.latitude
  # fill_in 'Longitude', with: @post.longitude
  # fill_in 'Contactname', with: @post.contactname
  # fill_in 'Apartmentorroom', with: @post.apartmentorroom
  # fill_in 'Numberofroom', with: @post.numberofroom
  # fill_in 'Phone', with: @post.phone
  # click_button 'Create Post'
end

Then("I should see the details of my post") do
  # expect(page).to have_content "Title: #{@post.title}"
  # expect(page).to have_content "Description: #{@post.description}"
end
