Given("I am an Admin") do
  @admin = FactoryBot.create :admin
  @user = FactoryBot.create :user

end

Given("I am signed in") do
  visit '/'
  fill_in 'Email', with: @admin.email
  fill_in 'Password', with: @admin.password
  click_button 'Log in'
end

When("I visit the dasboard page") do
  visit posts_path
end
