Rails.application.routes.draw do
  devise_for :users
  resources :posts
  get 'ps3/index'
  resources :quotations

  get 'main/index'

  get 'ps1/index'
  get 'ps1/dzero'
  get 'ps1/gnews'

  get 'ps2/index'
  get 'ps2/quotations'
  post 'ps2/set_cookies'
  get 'ps2/delete_cookies'
  get 'ps2/import'
  get 'ps2/export'
  get 'ps2/savexml'
  post 'ps2/importquote'
  get 'posts/index'
  get 'ps3/showpdf'
  post 'posts/banuser'
  root 'main#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
