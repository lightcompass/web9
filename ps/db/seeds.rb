# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
@admin = Role.create(name: "Admin")
@user1 = Role.create(name: "User1")
User.create(email: "part@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            name: "Part Jaithong",
            address: "AIT",
            phone: "0622998332",
            sex: "Male",
            occupation: "Student",
            annualincome: "$12000",
            dob: "12/08/1991",
            maritalstatus: "Single",
            banstatus: "false",
            role: @admin)
User.create(email: "abhi@gmail.com",
            password: "123456",
            password_confirmation: "123456",
            name: "Abhishek Koirala",
            address: "Pathum Thani AIT",
            phone: "06232856598",
            sex: "Male",
            occupation: "System Admin",
            annualincome: "$20000",
            dob: "30/02/1990",
            maritalstatus: "Single",
            banstatus: "false",
            role: @user1)