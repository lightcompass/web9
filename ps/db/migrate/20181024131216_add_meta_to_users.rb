class AddMetaToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :meta, :text
  end
end
