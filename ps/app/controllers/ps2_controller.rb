class Ps2Controller < ApplicationController
  require 'nokogiri'
  def index

  end

  def quotations

    @cookies = cookies.permanent[:kill_list]

    # New Quote
    if params[:quotation]
      @quotation = Quotation.new( params[:quotation] )
      if @quotation.save
        flash[:notice] = 'Quotation was successfully created.'
        @quotation = Quotation.new

      end

    else
      @quotation = Quotation.new
    end

    if params[:sort_by] == "date"
      @quotations = Quotation.order(:created_at)
    else
      @quotations = Quotation.order(:category)
    end

    # Search
    if params[:term]
      if cookies.permanent[:kill_list].present?
        @quotations = Quotation.where("id NOT IN (#{cookies.permanent[:kill_list]}) AND author_name LIKE ?", "%#{params[:term]}%")
      else
        @quotations = Quotation.where('author_name LIKE ?', "%#{params[:term]}%")

      end
      @category_quote = Quotation.all.pluck(:category)

      if @quotations.blank? || params[:term] == ""
        redirect_to ps2_quotations_path , notice: "No Quotation found with given '#{params[:term]}' keyword!"
        #flash[:notice] = 'No Quotation found with given keyword'

      end
    end

    # Personalize
    if cookies.permanent[:kill_list].present?
      @quotations = Quotation.where("id NOT IN (#{cookies.permanent[:kill_list]})")
      @category_quote = Quotation.all.pluck(:category)
      if @quotations.blank?
        flash[:notice] = 'No more Quotation to hide'
      end

    end
    respond_to do |format|
      format.html
      format.xml{ render xml: @quotations}
      format.json { render json: @quotations }
    end


  end

  def set_cookies
    #cookies[:kill_list]   = params[:id]
    if cookies.permanent[:kill_list].present?
      if !cookies.permanent[:kill_list].include? params[:id]
        cookies.permanent[:kill_list]   = cookies.permanent[:kill_list]+", "+params[:id]
      end

    else
      cookies.permanent[:kill_list]   = params[:id]

    end
# edited
    redirect_to ps2_quotations_path, notice: "You will not see it unless you reset"
    #end

  end
  helper_method :set_cookies

  def show_cookies
    @cookies    = cookies[:kill_list]

  end
  helper_method :show_cookies

  def delete_cookies
    redirect_to ps2_quotations_path, notice: "You will see all Quote"
    cookies.delete :kill_list

  end

  def import



  end

  def upload


  end

  def xml
    respond_to do |format|

      format.xml { render xml: @quotations }
    end
  end

  def json
    respond_to do |format|

      format.json { render json: @quotations }
    end
  end

  def importquote



    if !params[:file].blank?

      doc = Nokogiri::XML(params[:file])

      doc.css('quotation').each do |node|
        children = node.children

        Quotation.create(
            :author_name => children.css('author-name').inner_text,
            :category => children.css('category').inner_text,
            :quote => children.css('quote').inner_text
        )

      end


      redirect_to ps2_quotations_path, notice: "Import successfully"
    else
      redirect_to ps2_quotations_path, notice: "No json Input please try again"
    end
  end

end
