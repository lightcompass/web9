class Ps1Controller < ApplicationController
  # Method for Google news
  def index

  end

  def gnews
    require 'mechanize'
    require 'nokogiri'
    require 'open-uri'

    mechanize = Mechanize.new

    @news_title = Array.new(2) {Array.new()}
    @news_link_unfilter = Array.new(2) {Array.new()}
    @news_link = Array.new(2) {Array.new()}
    @news_img = Array.new(2) {Array.new()}
    @topic = Array.new
    @page = Array.new

    #article_class = "NiLAwe gAl5If jVwmLb Oc0wGc R7GTQ keNKEd j7vNaf nID9nc"

    # Lastest URL
    @page[0] = "https://news.google.com/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNRFZxYUdjU0FtVnVHZ0pWVXlnQVAB?hl=en-US&gl=US&ceid=US%3Aen"

    # World URL
    @page[1] = "https://news.google.com/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNRGx1YlY4U0FtVnVHZ0pWVXlnQVAB?hl=en-US&gl=US&ceid=US%3Aen"


    # Number of item to fetch
    @number_of_article = 5

    j = 0
    @page.each do |value|

      # AIT proxy for fixing Time out Error with HTTP request
      retreived = open(@page[j],
                       proxy: URI.parse("http://192.41.170.23:3128")
      )

      retreived = Nokogiri::HTML::Document.parse(retreived)

      i = 0
      # Topic Title
      @topic[j] = retreived.css("title")[0].text

      # News item
      while i < @number_of_article do
        i

        @news_title[j][i] = retreived.css('.NiLAwe.gAl5If.jVwmLb.Oc0wGc.R7GTQ.keNKEd.j7vNaf.nID9nc article h3 span')[i].text
        @news_title[j][i] = retreived.css('.NiLAwe.gAl5If.jVwmLb.Oc0wGc.R7GTQ.keNKEd.j7vNaf.nID9nc article h3 span')[i].text
        @news_link_unfilter[j][i] = retreived.css(".NiLAwe.gAl5If.jVwmLb.Oc0wGc.R7GTQ.keNKEd.j7vNaf.nID9nc article h3 a")[i]["href"]
        @news_link_unfilter[j][i][0, 1] = ''
        @news_link[j][i] = @news_link_unfilter[j][i]
        @news_img[j][i] = retreived.css('.NiLAwe.gAl5If.jVwmLb.Oc0wGc.R7GTQ.keNKEd.j7vNaf.nID9nc figure.AZtY5d img')[i]["src"]

        i += 1
      end

      j += 1
    end

  end

  # Method for Divide
  def dzero
    logger.info "About to divide by 0"
    10 / 0
  end

  helper_method :dzero

end
