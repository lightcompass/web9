class Ability
  include CanCan::Ability

  def initialize(user)
    def initialize(user)
      user ||= User.new # guest user (not logged in)
      if user.admin?
        can :read, :Post
        can :create, Post
        can :update, Post
        can :destroy, Post
        can :read, :User
        can :create, User
        can :update, User
        can :destroy, User

      else
        user.user1?
        can :read, Post
        can :create, Post
        can :update, Post do |post|
          post.try(:user) == user
        end
        can :destroy, Post do |post|
          post.try(:user) == user
        end
      end

    end
  end
end
