class Quotation < ApplicationRecord
  attr_accessor :category_new
  #validates_uniqueness_of :category
  #validates :category1, presence: true, if: -> { !category1.present? }
  #validates :category, uniqueness: { scope: :category1 }
  validates :author_name, presence: true
  validates :quote, presence: true

  def show

  end

  def Quotation.groups
    #return Quotation.find(:all, :select => "DISTINCT category")
    return find_by_sql("SELECT DISTINCT category from quotations; ")
  end

end
