Given("I am a user") do
  @user = FactoryBot.create :admin
end

Given("I want to add a post") do
  @post = FactoryBot.build :post
end

Given("I am signed in") do
  visit new_user_session_path
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
end

When("I visit the post page") do

  visit posts_path

end

Then("I should see a link for the adding the post") do

  expect(page).to have_link('New Post', href: new_post_path)
end

When("I click the link for the adding post") do
  find_link('New Post', href: new_post_path).click

end

Then("I should see a form to add a post") do
  expect(page).to have_selector('form#new_post')
end

When("I submit the form") do
  fill_in 'Posttext', with: @post.posttext
  click_button 'Create Post'

end

Then("I should see the details of my post") do
 visit posts_path
  expect(page).to have_content "#{@post.posttext}"
end
