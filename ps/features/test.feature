Feature: Post
  Scenario: Add post
  A  should be able to add students to a project.
    Given I am a user
    And I want to add a post
    And I am signed in
    When I visit the post page
    Then I should see a link for the adding the post
    When I click the link for the adding post
    Then I should see a form to add a post
    When I submit the form
    Then I should see the details of my post
