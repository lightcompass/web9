FactoryBot.define do
  factory :admin, class: User do
    email "user6@ait.asia"
    password "password"
    password_confirmation "password"
    name "ash"
    address "asjhj"
    phone "ajsd"
    sex "hjasd"
    occupation "213"
    annualincome "123"
    dob "qwe"
    maritalstatus "qwe"
    role Role.create(name: "Admin")
  end
  factory :post, class: Post do
    posttext "asdkas"
  end
end