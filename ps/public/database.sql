/*Query for altering default datestyle to MM/DD/YYYY */ 
DROP TABLE IF EXISTS my_stocks;
DROP TABLE IF EXISTS stock_prices;
DROP TABLE IF EXISTS newly_acquired_stocks;

ALTER DATABASE "ps2_development" SET datestyle TO "SQL, MDY";

/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 7 */

CREATE TABLE my_stocks
	(
	  symbol        VARCHAR(20) NOT NULL,
	  n_shares      INTEGER NOT NULL,
	  data_acquired DATE NOT NULL
	);

/* Populating my_stocks FROM '{projectfolderlocation}/data.txt' WITH (format 'text')*/
\copy my_stocks FROM '/home/web9/ps/public/data.txt' WITH (format 'text');

/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 8 */

/* Using only one SQL statement, create a table called stock prices with three columns: symbol, quote date, and
price. Within this one statement, fill the table you’re creating with one row per symbol in my stocks.
The date and price columns should be filled with the current date and a nominal price. Hint: select
symbol, sysdate as quote date, 31.415 as price from my stocks; */

CREATE TABLE stock_prices AS
SELECT symbol,
       CURRENT_DATE AS quote_date,
       31.415       AS price
FROM   my_stocks;


/* Create a new table newly_acquired_stocks using a single insert into ... select ... statement (with a WHERE clause appropriate to your sample
data), copy about half the rows from my stocks into newly acquired stocks.*/

CREATE TABLE newly_acquired_stocks
     (
                  symbol        VARCHAR(20) NOT NULL,
                  n_shares      INTEGER NOT NULL,
                  data_acquired DATE NOT NULL
     );


INSERT INTO newly_acquired_stocks
    (
       SELECT *
       FROM   my_stocks
       WHERE  n_shares>=1
    );


/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 9 */

/* With a single SQL statement JOINing my stocks and stock prices, produce a report showing symbol, number of shares, price per share, and current value. */

SELECT m.symbol,
       m.n_shares,
       n.price,
       m.n_shares*n.price AS current_value
FROM   my_stocks m
join   stock_prices n
ON     m.symbol=n.symbol;


/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 10 */

/* Insert a row into my stocks. Rerun your query from the previous exercise. Notice that your new stock does not appear in the report. This is because you’ve JOINed them with the constraint that the symbol appear in both tables. Modify your statement to use an OUTER JOIN instead so that you’ll get a complete report of all your stocks, but won’t get price information if none is available. */ 

INSERT INTO my_stocks VALUES
            (
	            'FOC',
	            12,
	            CURRENT_DATE
            );

SELECT
	m.symbol,
	m.n_shares,
	n.price
FROM 
    my_stocks m
FULL OUTER join 
	stock_prices n
ON              
	m.symbol=n.symbol;


/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 11 */

/* Inspired by Wall Street’s methods for valuing Internet companies, we’ve developed our own valuation method for this problem set: a stock is valued at the sum of the ASCII characters making up its symbol. (Note that students who’ve used lowercase letters to represent symbols will have higher-valued portfolios than those who’ve used all-uppercase symbols; “IBM” is worth only $216, whereas “ibm” is worth $312!) Define a PL/SQL function that takes a trading symbol as its argument and returns the stock value. */

/* Creating a sql function getValue with symbol as parameter */
CREATE or replace FUNCTION getValue(symbol IN varchar(10))
   RETURNS integer AS $sumprice$
   DECLARE
    symbols INTEGER;
    sumprice INTEGER;
   BEGIN
   sumprice = 0;
   	FOR i IN 1..length(symbol) LOOP
   		SELECT ASCII(SUBSTRING( symbol ,i , 1 )) into symbols;
   		SELECT sumprice+symbols into sumprice;
   	end loop;
   	RETURN sumprice;
 END;
$sumprice$ LANGUAGE plpgsql;

/* With a single UPDATE statement, update stock prices to set each stock’svalue to whatever is returned by this PL/SQL procedure. */

/* calling getValue() with symbol as parameter */
UPDATE stock_prices set price = getValue(symbol);


/* Define a PL/SQL function that takes no arguments and returns the aggregate value of the portfolio (n shares * price for each stock). You’ll
want to define your JOIN from DB Exercise 3 (above) as a cursor and then use the PL/SQL Cursor FOR LOOP facility. Hint: when you’re all done, you can run this procedure from the psql client with select portfolio value() from dual;.*/

CREATE OR REPLACE FUNCTION portfolio_value()
RETURNS SETOF varchar AS $$
DECLARE
  curs CURSOR FOR SELECT one.n_shares*two.price as agg_val FROM my_stocks one join stock_prices two ON one.symbol=two.symbol;
  row RECORD;
BEGIN
  open curs;
  LOOP
    FETCH FROM curs INTO row;
    EXIT WHEN NOT FOUND;
    return next row.agg_val;
  END LOOP;
END; 
$$ LANGUAGE plpgsql;

/* _________________________________________________________________________________________________________________________________________*/

/* Solution for QUESTION NUMBER 12 */

/*  */
INSERT INTO my_stocks
(SELECT one.symbol,
        one.n_shares * 2 AS n_shares,
        current_date   AS data_acquired
 FROM   my_stocks one
        join stock_prices two
          ON one.symbol = two.symbol
 WHERE  two.price >= (SELECT Avg(price)
                    FROM   stock_prices));


SELECT one.symbol,
       SUM(one.n_shares)           AS total_shares_held,
       SUM(one.n_shares * two.price) AS total_value
FROM   my_stocks one
       join stock_prices two
         ON one.symbol = two.symbol
GROUP  BY one.symbol
HAVING Count(one.symbol) > 1;


/* _________________________________________________________________________________________________________________________________________*/


